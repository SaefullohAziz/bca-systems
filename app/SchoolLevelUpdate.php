<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Datakrama\Eloquid\Traits\Uuids;

class SchoolLevelUpdate extends Pivot
{
    use Uuids;

    /**
     * Get the school that owns the level update.
     */
    public function school()
    {
        return $this->belongsTo('App\School');
    }

    /**
     * Get the level that owns the level update.
     */
    public function level()
    {
        return $this->belongsTo('App\SchoolLevel', 'school_level_id');
    }

    /**
     * Get the staff that owns the level update.
     */
    public function staff()
    {
        return $this->belongsTo('App\Admin\User', 'staff_id');
    }

    /**
     * Get the staff that owns the level update.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
