<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Znck\Eloquent\Traits\BelongsToThrough;
use Datakrama\Eloquid\Traits\Uuids;

class SchoolStatusUpdate extends Pivot
{
    use Uuids, BelongsToThrough;
    
    /**
     * Get the school that owns the status update.
     */
    public function school()
    {
        return $this->belongsTo('App\School');
    }

    /**
     * Get the status that owns the status update.
     */
    public function status()
    {
        return $this->belongsTo('App\SchoolStatus', 'school_status_id', 'id');
    }

    /**
     * Get the level that owns the status update.
     */
    public function level()
    {
        return $this->belongsToThrough('App\SchoolLevel', 'App\SchoolStatus');
    }

    /**
     * Get the staff that owns the status update.
     */
    public function staff()
    {
        return $this->belongsTo('App\Admin\User', 'staff_id');
    }

    /**
     * Get the staff that owns the status update.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * Get the attendance status for the school status update.
     */
    public function attendanceStatus()
    {
        return $this->hasOne('App\AttendanceStatus');
    }

    /**
     * Get the visitation destination update for the school status update.
     */
    public function visitationDestinationUpdate()
    {
        return $this->hasOne('App\VisitationDestinationUpdate');
    }

    /**
     * The school that belong to the school status update.
     */
    public function schoolVisitationDestination()
    {
        return $this->belongsToMany('App\School', 'visitation_destination_updates', 'school_status_update_id', 'school_id')->using('App\VisitationDestinationUpdate')->withTimestamps();
    }
}
