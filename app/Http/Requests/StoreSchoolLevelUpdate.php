<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StoreSchoolLevelUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $count = 1;
        if ($this->filled('school_id')) {
            $count = count($this->get('school_id'));
        }
        $rules = [
            'school_id' => [
                'required',
                'array',
                'min:1'
            ],
            'school_id.*' => [
                'required',
                'min:1'
            ],
            'level_id' => [
                Rule::requiredIf($this->get('school_id')),
                'array',
                'min:' . $count
            ],
            'level_id.*' => [
                Rule::requiredIf($this->get('school_id')),
                'min:' . $count
            ],
        ];
        return $rules;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'school_id' => 'school',
            'school_id.*' => 'school',
            'level_id' => 'level',
            'level_id.*' => 'level'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'level_id.*.required'  => 'All :attribute is required.',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->filled('school_id')) {
                $schools = \App\School::whereIn('id', $this->get('school_id'))->pluck('name')->toArray();
                collect($schools)->each(function ($item, $key) {
                    request()->request->add(['school_name[' . $key . ']' => $item]);
                });
            }
        });
    }
}
