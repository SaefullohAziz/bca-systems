<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Datakrama\Eloquid\Traits\Uuids;

class VisitationDestinationUpdate extends Pivot
{
    use Uuids;

    /**
     * Get the school status update that owns the visitation destination update.
     */
    public function schoolStatusUpdate()
    {
        return $this->belongsTo('App\SchoolStatusUpdate');
    }

    /**
     * Get the school that owns the visitation destination update.
     */
    public function school()
    {
        return $this->belongsTo('App\School');
    }
}
