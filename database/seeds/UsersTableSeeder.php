<?php

use Illuminate\Database\Seeder;
use App\Admin\User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $supersu = User::create([
        	'username' => 'supersu',
        	'name' => 'Supersu',
        	'email' => 'supersu@example.com',
        	'password' => Hash::make('rememberthat')
        ]);
        $supersu->assignRole('supersu');

        $admin = User::create([
        	'username' => 'admin',
        	'name' => 'Admin',
        	'email' => 'admin@example.com',
        	'password' => Hash::make('rememberthat')
        ]);
        $admin->assignRole('admin');

        $finance = User::create([
        	'username' => 'finance',
        	'name' => 'Finance',
        	'email' => 'finance@example.com',
        	'password' => Hash::make('rememberthat')
        ]);
        $finance->assignRole('finance');

        $user = User::create([
        	'username' => 'user',
        	'name' => 'User',
        	'email' => 'user@example.com',
        	'password' => Hash::make('rememberthat')
        ]);
        $user->assignRole('user');

        // user migration
        $admin = User::create([
            'username' => 'susanto.rahsito',
            'name' => 'Susanto Rahsito',
            'email' => 'susanto.rahsito@mitraabadi.com',
            'password' => Hash::make('administrator')
        ]);
        $admin->assignRole('admin');

        $admin = User::create([
            'username' => 'imanuelarifin',
            'name' => 'Imanuel Arifin',
            'email' => 'imanuelarifin@gmail.com',
            'password' => Hash::make('administrator')
        ]);
        $admin->assignRole('admin');

        $admin = User::create([
            'username' => 'yoshua',
            'name' => 'Yoshua Hutagalung',
            'email' => 'yoshuahutagalung@gmail.com',
            'password' => Hash::make('administrator')
        ]);
        $admin->assignRole('admin');

        $admin = User::create([
            'username' => 'rensofenius',
            'name' => 'Renso Fenius',
            'email' => 'rensofenius@gmail.com',
            'password' => Hash::make('administrator')
        ]);
        $admin->assignRole('admin');
    }
}
