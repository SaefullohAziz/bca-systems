<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitationDestinationUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitation_destination_updates', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('school_status_update_id')->index();
            $table->foreign('school_status_update_id')->references('id')->on('school_status_updates')->onDelete('cascade')->onUpdate('cascade');
            $table->uuid('school_id')->index();
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitation_destination_updates');
    }
}
