<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('type');
            $table->string('name');
            $table->text('address');
            $table->string('province');
            $table->string('regency');
            $table->string('police_number')->nullable();
            $table->string('since')->nullable();
            $table->string('school_phone_number')->nullable();
            $table->string('school_email');
            $table->string('school_web');
            $table->string('total_student');
            $table->string('acp_student')->nullable();
            $table->string('department');
            $table->string('iso_certificate')->nullable();
            $table->string('mikrotik_academy')->nullable();
            $table->string('headmaster_name')->nullable();
            $table->string('headmaster_phone_number')->nullable();
            $table->string('headmaster_email')->nullable();
            $table->string('proposal')->nullable();
            $table->string('reference');
            $table->string('dealer_name');
            $table->string('dealer_phone_number');
            $table->string('dealer_email');
            $table->string('document')->nullable();
            $table->string('notif')->nullable();
            $table->string('code');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
