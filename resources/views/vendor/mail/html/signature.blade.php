<table class="" width="100%" cellpadding="2" cellspacing="0" role="presentation">
    <tr>
        <td colspan=2>
            {{ Illuminate\Mail\Markdown::parse($slot) }}

            <p>Apabila ada pertanyaan atau kurang jelas Bapak / Ibu bisa menghubungi PIC BCA-CMA di +622129641400 pada jam operasional Senin s/d Sabtu Pk. 08.30 s/d 16.30 WIB.</p>  

            <p>Salam Pendidikan.</p>

            <p><strong>Hormat Kami</strong></p>

            <p><strong>Team BCA Cash Management Academy</strong></p>
        </td>
    </tr>
    <tr>
        <td>
            <img src="{{ asset('img/mails/bca-logo-sm.png') }}" alt="" margin="2">
        </td>
        <td>
            <small>
                <strong>BCA Cash Management Academy</strong>
                <br>Office: +62 21 296 414 00 
                <br>Fax: +62 21 296 414 99
                <br>Web: www.bcateachingfactory.com | E-Mail: informasi@bcateachingfactory.com
            </small>
        </td>
    </tr>
</table>
